import torch
import numpy as np
import torchvision.transforms as transforms
import torchvision.datasets as dsets

def get_data(path, batch_size):
    """
    Transforms the dataset and creates a dataloader for the specified path and batch size.
    
    Args
    ---------------
    path: The path of the folder that contains the dataset
    batch_size: The size of the batches that the data will be loaded in
    
    Returns
    ---------------
    Dataloader: The dataloader for the dataset with the specified path and batch size.
    """
    
    # Create a pipeline of transforms for the given dataset
    transform = transforms.Compose([
        # Resize the images such that the smaller edge is 32 pixels and the aspect ratio stays the same
        transforms.Resize(size=32),
        # Crop the images such that they are 32x32 pixels
        transforms.CenterCrop(size=32),
        # Transform the data from PIL images to PyTorch tensors
        transforms.ToTensor(),
        # Normalise the data such that the mean and standard deviation of each dimension of the images is 0.5
        transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))])
    
    # Load the data from the specified folder and apply the transforms
    dataset = dsets.ImageFolder(root=path, transform=transform)
    
    # Load the data in batches with the specified batch size and reshuffle it after each epoch 
    dataloader = torch.utils.data.DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True)
    
    return dataloader

def get_latent_input(batch_size, num_discrete, discrete_dim, num_z, device):
    """
    Noise input for generator
    
    Args:
    ---------------
    batch_size: size of batch noise to be generated for
    num_discrete: Number of discrete latent code per datapoint
    discrete_dim: dimnesion of discrete latent code
    num_z:  Dimension of incompressible noise (Random normal noise)
    device: tensor location CPU/GPU

    Returns:
    ---------------
    latent_input: latent input vector and set of selected discrete codes for every datapoint.
    """
    # stub for selected discrete codes for every data point
    codes = np.zeros((num_discrete, batch_size))
    # noise stub for discrete codes for every data point
    discrete_code = torch.zeros(size=(batch_size, num_discrete, discrete_dim), device=device)
    # for every discrete code
    for i in range(num_discrete):
        # get selected discrete codes
        codes[i] = np.random.randint(discrete_dim, size=batch_size)
        # set non zero values for selected discrete codes for all data points
        discrete_code[range(0, batch_size), i, codes[i]] = 1.0
    # reshape noise
    discrete_code = discrete_code.view(batch_size, -1, 1, 1)
    # generate random normal size for every data point in batch
    normal_noise = torch.randn(batch_size, num_z, 1, 1, device=device)
    # combine random normal noise with discrete codes
    latent_input = torch.cat(tensors=(normal_noise, discrete_code), dim=1)
    return latent_input, codes

def initialize_weights(layer):
    """
    Ref: https://github.com/pianomania/infoGAN-pytorch
    
    Initialise weights for a given layer in a network.
    
    Args:
    ---------------
    layer: A network layer
    """
    classname = layer.__class__.__name__
    if classname.find('Conv') != -1:
        layer.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        layer.weight.data.normal_(1.0, 0.02)
        layer.bias.data.fill_(0)

