#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import random
from copy import deepcopy


# In[2]:


model_path = './checkpoint/model_epoch_10_CelebA'
state_dict = torch.load(model_path)


# In[3]:


from Models.Generator import Generator


# In[4]:


device = torch.device("cuda:0" if(torch.cuda.is_available()) else "cpu")


# In[5]:


generator = Generator().to(device)
generator.load_state_dict(state_dict['netG'])
print(generator)


# In[6]:


seed = 228
random.seed(seed)
torch.manual_seed(seed)
print("Random Seed: ", seed)


# In[7]:


# set noise parameters
num_z = 128
num_discrete = 10
discrete_dim = 10


# In[8]:


# Generate Fixed Noise
# get discrete codes
num_images = 100
idx = np.arange(discrete_dim).repeat(10)
discrete_codes = torch.zeros(size=(num_images, num_discrete, discrete_dim), device=device)
for i in range(num_discrete):
    discrete_codes[range(0, num_images), i, idx] = 1.0
    
discrete_codes = discrete_codes.view(num_images, -1, 1, 1)
# get normal noise
normal_noise = torch.randn(num_images, num_z, 1, 1, device=device)

# Fixed noise
fixed_noise = torch.cat((normal_noise, discrete_codes), dim=1)


# In[9]:


# noises with all features set to 1
my_noise = deepcopy(fixed_noise[0:10])


# In[10]:


# modify noise
for noise in my_noise:
    # position of 1's are 138, 148..
    # set 1's to zeros
    noise[138][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
    # add suitable number to 1's postion to shift index to other class and set it to 1
    noise[140][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)
#     noise[148][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
#     noise[150][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)
#     noise[158][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
#     noise[161][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)
#     noise[168][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
#     noise[175][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)
#     noise[178][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
#     noise[180][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)
#     noise[188][0][0] = torch.tensor(data=0, dtype=torch.float32, device=device)
#     noise[190][0][0] = torch.tensor(data=1, dtype=torch.float32, device=device)


# In[11]:


# all one's result
with torch.no_grad():
    generated_img1 = generator(fixed_noise[0:10]).detach().cpu()
fig = plt.figure(figsize=(20, 15))
plt.axis("off")
plt.imshow(np.transpose(vutils.make_grid(generated_img1, nrow=10, padding=2, normalize=True), (1,2,0)))
plt.show()


# In[12]:


# modified result
with torch.no_grad():
    generated_img1 = generator(my_noise).detach().cpu()
# Display the generated image.
fig = plt.figure(figsize=(20, 15))
plt.axis("off")
plt.imshow(np.transpose(vutils.make_grid(generated_img1, nrow=10, padding=2, normalize=True), (1,2,0)))
plt.show()

