import torch
import torch.nn as nn
import torch.nn.functional as F


class DHead(nn.Module):
    """
    DHead Class for the InfoGAN as presented in "InfoGAN: Interpretable Representation Learning by
    Information Maximizing Generative Adversarial Nets" 2016 (Chen, Duan, Houthfoot, Schulman, Sutskever, Abbeel)
    """
    def __init__(self):
        super().__init__()

        #Convolutional layer 1
        self.conv=nn.Conv2d(in_channels=256,out_channels=1,kernel_size=4)

    def forward(self, x):
        #4x4 convolutional layer, 256 inputs, sigmoid output
        output = torch.sigmoid(self.conv(x))
        
        return output
