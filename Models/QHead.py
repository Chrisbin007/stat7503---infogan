import torch
import torch.nn as nn
import torch.nn as nn
import torch.optim as optim
import torchvision.utils as vutils
import torch.nn.functional as F

import torchvision
import torchvision.transforms as transforms

class QHead(nn.Module):
    """
    QHead Class for the InfoGAN as presented in "InfoGAN: Interpretable Representation Learning by
    Information Maximizing Generative Adversarial Nets" 2016 (Chen, Duan, Houthfoot, Schulman, Sutskever, Abbeel)
    """
    def __init__(self):
        super().__init__()
        
        #Convolutional layer 1
        self.c1=nn.Conv2d(in_channels=256,out_channels=128,kernel_size=4)
        #Batch Normalisation layer 1
        self.b1=nn.BatchNorm2d(num_features=128)
        #Convolutional layer 2
        self.c2=nn.Conv2d(in_channels=128,out_channels=100,kernel_size=1)
        
    
    def forward(self, x):
        
        #4x4 convolutional layer, 256 inputs
        x=F.relu(self.b1(self.c1(x)))
        #1x1 convolutional layer, 128 inputs
        logits=self.c2(x).squeeze()

        return logits
