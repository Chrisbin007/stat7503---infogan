import torch.nn as nn
import torch.nn.functional as F


class Discriminator(nn.ModuleList):
    """
    Discriminator Class for the InfoGAN as presented in "InfoGAN: Interpretable Representation Learning by
    Information Maximizing Generative Adversarial Nets" 2016 (Chen, Duan, Houthfoot, Schulman, Sutskever, Abbeel)
    """
    def __init__(self):
        super(Discriminator, self).__init__()
        """
        Defining convolutional layers and batch-norm layers
        """
        
        self.c1=nn.Conv2d(in_channels=3,out_channels=64,kernel_size=4,stride=2,padding=1)
        #Convolutional layer 2
        self.c2=nn.Conv2d(in_channels=64,out_channels=128,kernel_size=4,stride=2,padding=1)
        #Batch Normalisation layer 1
        self.b1=nn.BatchNorm2d(num_features=128)
        #Convolutional layer 3
        self.c3=nn.Conv2d(in_channels=128,out_channels=256,kernel_size=4,stride=2,padding=1)
        #Batch Normalisation layer 2
        self.b2=nn.BatchNorm2d(num_features=256)
        
        
    def forward(self, x):
        """
        Applying the forward pass
        """
        
        #4x4 convolutional layer, 64 inputs, stride 2
        x=F.relu(self.c1(x))
        #4x4 convolutional layer, 128 inputs, stride 2
        x=F.relu(self.b1(self.c2(x)))
        #4x4 convolutional layer, 256 inputs, stride 2
        x=F.relu(self.b2(self.c3(x)))
        
        return x