import torch
import torch.nn as nn
import torch.nn.functional as F

"""
Generator module

"""
class Generator(nn.Module):
    """
    Generator Class for the InfoGAN as presented in "InfoGAN: Interpretable Representation Learning by
    Information Maximizing Generative Adversarial Nets" 2016 (Chen, Duan, Houthfoot, Schulman, Sutskever, Abbeel)
    """
    def __init__(self):
        super(Generator, self).__init__()
        # ConvTranspose layer 1
        self.conv_t1 = nn.ConvTranspose2d(in_channels=228, out_channels=448, kernel_size=2, stride=1, bias=False)
        # Batch norm 1
        self.batch_norm1 = nn.BatchNorm2d(num_features=448)
        # ConvTranspose layer 2
        self.conv_t2 = nn.ConvTranspose2d(in_channels=448, out_channels=256, kernel_size=4, stride=2, padding=1, bias=False)
        # Batch norm 2
        self.batch_norm2 = nn.BatchNorm2d(num_features=256)
        # ConvTranspose layer 3
        self.conv_t3 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1, bias=False)
        # ConvTranspose layer 4
        self.conv_t4 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False)
        # ConvTranspose layer 5
        self.conv_t5 = nn.ConvTranspose2d(in_channels=64, out_channels=3, kernel_size=4, stride=2, padding=1, bias=False)
    
    def forward(self, x):
        # define forward pass for the network
        
        # FC. 2 × 2 × 448 RELU. batchnorm.
        x = self.batch_norm1(self.conv_t1(x))
        x = F.relu(x)
        # 4 × 4 upconv. 256 RELU. stride 2. batchnorm.
        x = self.batch_norm2(self.conv_t2(x))
        x = F.relu(x)
        # 4 × 4 upconv. 128 RELU. stride 2.
        x = F.relu(self.conv_t3(x))
        # 4 × 4 upconv. 64 RELU. stride 2.
        x = F.relu(self.conv_t4(x))
        # 4 × 4 upconv. 3 Tanh. stride 2.
        output = torch.tanh(self.conv_t5(x))
        
        return output
